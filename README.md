###Live Demo - [http://intro.codedemon.in](http://intro.codedemon.in)###

###Setup Local (Ubuntu 14.04) ###
## Software packages ##
1. sudo apt-get install libmysqlclient-dev git mysql-server
2. Install Ruby using [RVM](https://rvm.io/rvm/install)
    * gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    * \curl -sSL https://get.rvm.io | bash
    * source /path/to/rvm_install (as printed in install log)
    * rvm install 2.1.5
3. Install Rails
    * gem install rails 

## Building project ##
1. cd /project_location/
2. bundle install
3. update your mysql username and password: in config/database.yml
4. Set up database
    * RAILS_ENV=production rake db:create
    * RAILS_ENV=production rake db:migrate
5. Start server
    * foreman start (runs the command in "Procfile")

## Accessing project ##
1.  [http://localhost:3000](http://localhost:3000)
2.  Manager's username and password is "admin" and "novanet".

##Todo##
1.  Cache the response from http://www.timeanddate.com/holidays/india/ for public holiday