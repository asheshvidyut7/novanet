class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :name
      t.string :username
      t.string :password_digest
      t.boolean :role
      t.timestamps null: false
    end
    User.create(:name => "Novanet", :username => "admin", :password => "novanet", :role => true)

  end

  def down
    drop_table :users
  end
end
