class CreateLeaves < ActiveRecord::Migration
  def up
    create_table :leaves do |t|
      t.datetime :start
      t.datetime :end
      t.integer :user_id
      t.string :status
      t.string :subject
      t.integer :status
      t.integer :nodays
      t.string :comment
      t.timestamps null: false
    end
  end
  def down
    drop_table :leaves
  end
end
