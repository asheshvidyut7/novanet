class LeaveSerializer < ActiveModel::Serializer
  attributes :id, :start, :end, :user_id, :status, :subject, :nodays, :comment

  def attributes
    data = super
    data[:name] = User.find_by_id(object.user_id).name
    data
  end
end
