class User < ActiveRecord::Base

  has_secure_password
  has_many :leaves

  validates_presence_of :name
  validates_presence_of :username
  validates_presence_of :password, :on => :create
end
