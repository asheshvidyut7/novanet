class Api::LogoutController < ApplicationController
  def index
    session[:user_id] = nil
    session[:role] = nil
    session[:username] = nil
    render json: {:result => true}
  end
end
