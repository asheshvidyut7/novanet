class Api::SignupController < ApplicationController
  def index
    sparams = signup_params
    send_hash = {}
    if !User.find_by_username(sparams[:username]).nil?
      send_hash = {:result => false, :message => 'Username exists'}
    else
      user = User.new
      user.name = sparams[:name]
      user.username = sparams[:username]
      user.password = sparams[:password]
      user.role = false
      if user.save
        send_hash = {:result => true, :message => user.id}
      else
        send_hash = {:result => false, :message => 'Save error'}
      end
    end
    render :json => send_hash
  end
  private
  def signup_params
    params.permit(:name, :username, :password)
  end
end
