class Api::LoginController < ApplicationController
  def index
    lparams = loginparams
    username = lparams[:username]
    password = lparams[:password]
    begin
      user = User.find_by_username(username)
      send_hash = {}
      if user.authenticate(password)
        session[:user_id] = user.id
        session[:role] = user.role
        session[:username] = user.username
        send_hash = {:result => true, :user_id => user.id, :role => user.role, :user_name => user.username, :name => user.name}
      else
        send_hash = {:result => false, :message => 'Authentication error'}
      end
    rescue
      send_hash = {:result => false, :message => 'User not found'}
    end
    render :json => send_hash
  end

  def access
    if session[:role] == true
      send_hash = {:result => true}
    else
      send_hash = {:result => false}
    end
    render :json => send_hash
  end

  private
  def loginparams
    params.permit(:username, :password)
  end
end
