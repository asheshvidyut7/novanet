require 'date'
class Api::LeavesController < ApplicationController

  before_filter :logged_in?, :except => [:holiday]
  def index
    if session[:user_id].nil?
      render json: {}
    else
      if session[:role] == true
        leaves = Leave.all
      else
        leaves = Leave.where(:user_id => session[:user_id])
      end
      render json: leaves, each_serializer: LeaveSerializer, :root => false
    end

  end

  def create
    lparams = leaveparams
    user = User.find(session[:user_id])
    #checking number of working days to be correct
    unless validate_input(lparams[:start], lparams[:end], lparams[:nodays])
      send_hash = {:result => false, :message => 'Number of days not allowed'}
      render json: send_hash
      return
    end
    leave = Leave.new
    leave.start = Time.parse(lparams[:start])
    leave.end = Time.parse(lparams[:end])
    leave.nodays = lparams[:nodays]
    leave.subject = lparams[:subject]
    leave.save
    user.leaves << leave
    send_hash = {}
    if user.save
      send_hash = {:result => true}
    else
      send_hash = {:result => false, :message => 'Save Error.'}
    end
    render json: send_hash
  end

  def update
    if session[:role]
      id = params[:id]
      leave = Leave.find(id)
      leave.status = params[:status]
      leave.comment = params[:comment]
      if leave.save
        send_hash = {:result => true}
      else
        send_hash = {:result => false}
      end
      render json: send_hash
    else
      head :unauthorized
    end
  end

  def holidays (*year)
    year = params[:year] || year[0]
    url = "http://www.timeanddate.com/holidays/india/" + year.to_s
    page = Nokogiri::HTML(open(url))
    val = page.css('tbody tr th').map{|x| x.text + ', 2015'}.uniq
    if params[:year]
      render json: val 
    else
      return val
    end
  end

  private
  def logged_in?
    if session[:user_id].nil?
      head :unauthorized
    end
  end

  # @params start_time Start time entered by user 
  # @params end_time Start time entered by user 
  # @params num_of_days Number of days as calculated by javascript
  #
  # @return Boolean 
  
  def validate_input(start_time, end_time, num_of_days)
    # TODO: Shorten this function
    map = { "Jan" => 1, "Feb" => 2, "Mar" => 3, "Apr" => 4, "May" => 5, "Jun" => 6, "Jul" => 7, "Aug" => 8, "Sep" => 9, "Oct" => 10, "Nov" => 11, "Dec" => 12 }
    start_time = DateTime.parse(start_time)
    end_time = DateTime.parse(end_time)
    pub_holidays = holidays(end_time.year)
    pub_holidays = pub_holidays.map{ |i| i.gsub(",", "") }
    st_milli = start_time.strftime('%Q').to_i # start_time to milliseconds
    et_milli = end_time.strftime('%Q').to_i # end_time to milliseconds
    calculated_num_days = 0
    
    # Counts number of days excluding weekend
    while( st_milli <= et_milli )
      day = Time.at( st_milli / 1000 ).strftime("%w").to_i
      calculated_num_days += 1 if !(day == 0 || day == 6)
      st_milli += 24 * 60 * 60 * 1000
    end
    
    # Counts number of public holidays within the selected date range
    pub_holidays.length.times do |i|
      date = pub_holidays[i].split " "
      holiday = DateTime.new date[2].to_i, map[date[0]], date[1].to_i
      weekday = holiday.strftime('%w').to_i
      next if weekday == 0 || weekday == 6
      calculated_num_days -= 1 if holiday >= start_time && holiday <= end_time
    end
    
    return calculated_num_days == num_of_days
  end

  def leaveparams
    params.permit(:start, :end, :nodays, :subject)
  end
end
