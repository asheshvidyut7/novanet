(function (){
    var app = angular.module('holiday');
    app.factory('Leave', ['$http', function ($http){
        var url = '/api/leaves';
        function Leave(id, subject, start, end, noday, comment, status){
            this.id = id;
            this.subject = subject;
            this.start = start;
            this.end = end;
            this.nodays = noday;
            this.comment = comment;
            this.status = status;
            this.url = url + '/' + id;
        }
        Leave.prototype.get_all = function(){
            return $http.get(url).then(function(response){
                return response.data;
            }, function(error){
                return error;
            })
        };
        Leave.prototype.create = function(){
            var self = this;
            return $http({url : url, method : 'POST', data : self}).then(function(response){
                return response.data;
            }, function (error){
                return error;
            })
        };
        Leave.prototype.update = function(){
            var self = this;
            return $http({url : self.url, method : 'PUT', data : self}).then( function (response){
                return response.data;
            }, function (error){
                return error;
            })
        };
        return Leave;
    }]);
})();