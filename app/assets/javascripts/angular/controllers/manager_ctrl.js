(function (){
    var app = angular.module('holiday');
    app.controller('managerCtrl', ['$scope', 'access', 'Leave', function ($scope, access, Leave){
        //redirecting non manger access
        if(access == false){
            $state.go('login');
        }

        $scope.leaves = null;
        Leave.prototype.get_all().then(function (response){
            $scope.leaves = response;
        });

        $scope.update = function (status, leave){
            if(status == 2 && leave.comment == null){
                alert('Comment for rejection is mandatory.');
                return;
            }
            var index = $scope.leaves.indexOf(leave);
            var upleave = new Leave(leave.id, leave.subject, leave.start, leave.end, leave.nodays, leave.comment, leave.status);
            upleave.status = status;
            upleave.update().then(function (data){
                if(data.result == true){
                    leave.status = status;
                    $scope.leaves[index] = leave;
                }
                else{
                    console.log('Error');
                }
            });
        };
    }])
})();