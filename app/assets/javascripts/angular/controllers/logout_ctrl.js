(function (){
    var app = angular.module('holiday');
    app.controller('logoutCtrl', [ '$cookieStore', '$http', '$state', '$rootScope', function ($cookieStore, $http, $state, $rootScope){
        $http.get('/api/logout').then(function (response){
            var data = response.data;
            if(data.result == true){
                $cookieStore.remove('user_id');
                $cookieStore.remove('user_name');
                $cookieStore.remove('name');
                $cookieStore.remove('role');
                $rootScope.user_id = null;
                $rootScope.user_name = null;
                $rootScope.name = null;
                $rootScope.role = null;
                $state.go('login');
            }
        }, function (error){
            console.log(error);
        })
    }])
})();