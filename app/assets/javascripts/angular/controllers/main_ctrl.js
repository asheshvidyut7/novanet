(function (){
    var app = angular.module('holiday');
    app.controller('mainCtrl', ['$state', '$cookieStore', '$rootScope', function ($state, $cookieStore, $rootScope){
        if($cookieStore.get('user_id') == null){
            $state.go('login');
        }
        else{
            $rootScope.user_id = $cookieStore.get('user_id');
            $rootScope.user_name = $cookieStore.get('user_name');
            $rootScope.name = $cookieStore.get('name');
            $rootScope.role = $cookieStore.get('role');
            if($state.current.name === 'index'){
                if($rootScope.role){
                    $state.go('manager');
                }
                else{
                    $state.go('dashboard.home');
                }
            }
        }
    }]);
})();