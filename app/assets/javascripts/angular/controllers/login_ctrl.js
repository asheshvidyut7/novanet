(function (){
    var app = angular.module('holiday');
    app.controller('loginCtrl', ['$scope', '$timeout', '$http', '$cookieStore', '$rootScope', '$state', function ($scope, $timeout, $http, $cookieStore, $rootScope, $state){
        //already logged in
        if($rootScope.user_id != null){
            if($rootScope.role == true){
                $state.go('manager');
            }
            else{
                $state.go('dashboard.home');
            }
        }

        //login logic
        $scope.loginval = 'Login';
        $scope.loginclass = 'btn-primary';
        $scope.loginloading = false;
        $scope.login = function (user){
            $scope.loginloading = true;
            var params = {username : user.username, password : user.password};
            $http({url : '/api/login', method: 'POST', data : params}).then(function (response){
                $scope.loginloading = false;
                var data = response.data;
                if(data.result == true){
                    $cookieStore.put('user_id', data.user_id);
                    $cookieStore.put('user_name', data.user_name);
                    $cookieStore.put('name', data.name);
                    $cookieStore.put('role', data.role);
                    $rootScope.user_id = $cookieStore.get('user_id');
                    $rootScope.user_name = $cookieStore.get('user_name');
                    $rootScope.name = $cookieStore.get('name');
                    $rootScope.role = $cookieStore.get('role');
                    $scope.loginval = 'Logged In. Redirecting.';
                    $timeout(function(){
                        if($rootScope.role == true){
                            $state.go('manager');
                        }
                        else{
                            $state.go('dashboard.home');
                        }
                    }, 2000);
                }
                else {
                    $scope.loginval = data.message;
                    $timeout(function () {
                        $scope.loginval = 'Login';
                    }, 2000);
                }
            }, function (error){
                console.log(error);
            })
        };

        //signup logic
        $scope.signupval = 'Sign Up';
        $scope.signupclass = 'btn-success';
        $scope.signuploading = false;
        $scope.signup = function (nuser){
            $scope.signuploading = true;
            var params = {username : nuser.username, name : nuser.name, password : nuser.password};
            $http({url : '/api/signup', method: 'POST', data : params}).then(function (response){
                var data = response.data;
                $scope.signuploading = false;
                if(data.result == true){
                    $scope.signupval = 'Sign Up Done. Logging In..';
                    $scope.login(nuser);
                }
                else{
                    $scope.signupval = data.message;
                }
                $timeout(function(){
                    $scope.signupval = 'Sign Up';
                }, 3000);
            }, function (error) {
                console.log(error);
            });
        }
    }]);
})();