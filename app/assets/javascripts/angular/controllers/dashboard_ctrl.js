(function (){
    var app = angular.module('holiday');
    app.controller('dashboardCtrl', ['$rootScope', '$state', function ($rootScope, $state){
        //if not logged in
        if($rootScope.user_id == null){
            $state.go('login');
        }
    }]);
    app.controller('dashboardHomeCtrl', ['$scope', '$timeout', 'Leave', '$http', function ($scope, $timeout, Leave, $http){
        $scope.showstatus = true;
        $scope.status = "Loading public holidays";
        $scope.today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        //getting public holidays
        $scope.holidays = [];
        $scope.holidays.length = 1000;
        var year = $scope.today.getFullYear();
        $http({url : '/api/holidays', method : 'GET', params : {year : year}}).then(function (response){
            $scope.holidays = response.data.leaves;
            for(var i = 0; i < $scope.holidays.length; i++){
                $scope.holidays[i] = new Date($scope.holidays[i]).getTime();
            }
            updateValues();
            $scope.status = "Loading complete."
            $timeout(function (){
                $scope.showstatus = false;
            }, 1000);
        }, function(error){
            console.log(error);
        });
        $scope.mindate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $scope.maxdate = getMaxValidDate($scope.mindate);
        $scope.st = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $scope.et = $scope.maxdate;
        $scope.nd = calculateDaysWithoutHolidays($scope.st, $scope.et);

        $scope.butval = "Request";
        $scope.loading = false;

        //update selected range with 15 days max and calculate no of working days
        function updateValues(){
            $scope.mindate = $scope.st;
            $scope.maxdate = getMaxValidDate($scope.st);
            if($scope.maxdate < $scope.et){
                $scope.et = $scope.maxdate;
            }
            $scope.nd = calculateDaysWithoutHolidays($scope.st, $scope.et); 
        }

        $scope.$watchCollection('[st, et]', function(newval, oldval){
            updateValues();
        });

        //number of days from selected start and end
        function calculateDaysWithoutHolidays(st, et){
            var nodays = 0;
            while(st <= et){
                var index = $scope.holidays.indexOf(st.getTime());
                if(!(st.getDay() === 0 || st.getDay() === 6)){
                    if(index === -1){
                        nodays++;                        
                    }
                }
                st = new Date(st.getTime() + (24 * 60 * 60 * 1000));
            }
            return nodays;
        }
        //get date after 15 working day
        function getMaxValidDate(st){
            var numday = 0;
            var index = $scope.holidays.indexOf(st.getTime());
            if(!(st.getDay() === 0 || st.getDay() === 6)){
                if(index === -1){
                    numday = 1;
                }
            }
            while(numday != 15){
                st = new Date(st.getTime() + (24 * 60 * 60 * 1000));
                var index = $scope.holidays.indexOf(st.getTime());
                if(!(st.getDay() === 0 || st.getDay() === 6)){
                    if(index === -1){
                        numday++;
                    }
                }
            }
            // if the 15th day is followed by series of saturday, sunday , public holidays
            while(true){
                var nextday = new Date(st.getTime() + (24 * 60 * 60 * 1000));
                var index = $scope.holidays.indexOf(nextday.getTime());
                var holiday = false;
                if(nextday.getDay() === 0 || nextday.getDay() === 6 || index != -1){
                    st = nextday;
                }
                else{
                    break;
                }
            }
            return st;
        }

        $scope.create = function (){
            $scope.loading = true;
            var nleave = new Leave(null, $scope.subject, $scope.st, $scope.et, $scope.nd, null, null);
            nleave.create().then(function (response){
                $scope.loading = false;
                if(response.result == true){
                    $scope.butval = 'Requested.';
                    $timeout(function (){
                        $scope.butval = 'Request';
                    }, 2000)
                }
                else{
                    alert(response.message);
                }
            });
        }
    }]);
    app.controller('dashboardStatusCtrl', ['$scope', 'Leave', function ($scope, Leave){
        $scope.leaves = null;
        Leave.prototype.get_all().then(function (response){
            $scope.leaves = response;
        });
    }]);
})();