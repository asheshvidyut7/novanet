(function (){
    var app = angular.module('holiday', ['templates', 'ui.router', 'ngCookies', 'ui.bootstrap']);

    //configurations
    //csrf for ajax requests
    app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    }]);
    //routes
    app.config([ '$stateProvider', '$urlRouterProvider', function ( $stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider.state('index',{
            url : '/',
            controller : 'mainCtrl'
        });
        $stateProvider.state('login',{
            url : '/login',
            templateUrl : 'login.html',
            controller: 'loginCtrl'
        });
        $stateProvider.state('logout',{
            url : '/logout',
            controller: 'logoutCtrl'
        });
        $stateProvider.state('dashboard',{
            url : '/dashboard',
            templateUrl : 'dashboard.html',
            controller: 'dashboardCtrl'
        });
        $stateProvider.state('dashboard.home', {
            url : '/',
            templateUrl : 'dashboard.home.html',
            controller: 'dashboardHomeCtrl'
        });
        $stateProvider.state('dashboard.status', {
            url : '/status',
            templateUrl : 'dashboard.status.html',
            controller: 'dashboardStatusCtrl',
        });
        $stateProvider.state('manager',{
            url : '/manager',
            templateUrl : 'manager.html',
            controller: 'managerCtrl',
            resolve : {
                access : ['$http', function ($http){
                    return $http.get('/api/access').then(function (response){
                        var data = response.data;
                        return data.result;
                    }, function (error){
                        console.log(error);
                    })
                }]
            }
        });
    }]);

})();